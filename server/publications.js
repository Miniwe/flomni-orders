import { _ } from 'lodash';
import { check } from 'meteor/check';
import Orders from "../lib/collections/ordersCollection";
import Mailings from "../lib/collections/mailingsCollection";
import Contacts from "../lib/collections/contactsCollection";
import Segments from "../lib/collections/segmentsCollection";

Meteor.publish('orders', (query = {}, params = {}) => {
    check(query, Object);
    check(params, Object);
    params = _.extend({
        sort: {
            statusDate: -1
        }
    }, params);
    return Orders.find(query, params);
});

Meteor.publish('segments', (params = {}) => {
    check(params, Object)
    return Segments.find(params);
});

Meteor.publish('mailings', (params = {}) => {
    check(params, Object)
    return Mailings.find(params);
});

Meteor.publish('contacts', (params = {}) => {
    check(params, Object)
    return Contacts.find(params);
});

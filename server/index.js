import Fixtures from './fixtures';

Meteor.startup(function() {
    (new Fixtures()).populateCollections();
});
import Orders from '../../lib/collections/ordersCollection';

let CountersMethods = {
    getOrdersCount() {
        return Orders.find().count();
    }
}

Meteor.methods(CountersMethods);

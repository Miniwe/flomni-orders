import { Fake }  from "meteor/anti:fake";
import { _ }     from "lodash";

import Orders    from "../lib/collections/ordersCollection";
import Mailings  from "../lib/collections/mailingsCollection";
import Contacts  from "../lib/collections/contactsCollection";
import Segments  from "../lib/collections/segmentsCollection";

const orderStatus = {
    Draft: 'Черновик',
    New: 'Новый',
    Started: 'В процессе',
    Finished: 'Завершен',
    Completed: 'Закрыт',
    Deleted: 'Удален'
};

class Fixtures {
    constructor() { }

    populateOrders() {
        if (!Orders.find().count()) {
            for(let i=0; i<67; i++){
                const orderData = {
                    name: Fake.user().fullname,
                    summ: _.random(0,1000),
                    town: Fake.sentence(1),
                    address: `${Fake.sentence(3)} ${_.random(0,100)}`,
                    shop: `${Fake.word()}.${Fake.word().substring(0,3)}`,
                    status: Fake.fromArray(_.keys(orderStatus))
                };
                Orders.insert(orderData);
            }
        }
    }

    populateSegments() {
        if (!Segments.find().count()) {
            for(let i=0; i<7; i++){
                const segmentData = {
                    name: Fake.sentence(2)
                };
                Segments.insert(segmentData);
            }
        }
    }

    populateMailings() {
        if (!Mailings.find().count()) {
            // ...
        }
    }

    populateContacts() {
        if (!Contacts.find().count()) {
            // ...
        }
    }

    populateCollections() {
        this.populateOrders();
        this.populateSegments();
        this.populateMailings();
        this.populateContacts();
    }
}

export default Fixtures;
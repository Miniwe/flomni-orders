import { Template } from 'meteor/templating';
import Segments from '../../../../lib/collections/segmentsCollection';


Template.segmentsDropdown.onRendered(function() {
    this.$(".dropdown").dropdown({
        selector: {
            item: '.item-label'
        }
    });
});

Template.segmentsDropdown.helpers({
    segments() {
        return Segments.find();
    }
});

Template.segmentItem.events({
    'click [data-value=""]'(event, templateInstance) {
        templateInstance.$(".dropdown").dropdown('restore defaults');
    },
    'keyup .item-edit'(event, templateInstance) {
        let $item = $(event.currentTarget).parents('.item');
        const itemId = templateInstance.data.value;
        const value = $(event.currentTarget).val();

        if ( event.which == 13 || event.which == 27 ) {
            event.preventDefault();
            event.stopPropagation();
            if (event.which == 13) {
                Segments.update(itemId, {$set: {
                    name: value
                }});
            }
            $item.removeClass('edit');
            $(event.currentTarget).val($(".item-label", $item).text());
            return false;
        }
    },
    'click .ui.button'(event, templateInstance) {
        event.preventDefault();
        let $item = $(event.currentTarget).parents('.item');
        const itemId = templateInstance.data.value;

        if ($(event.currentTarget).hasClass('remove')){
            if (window.confirm('Удалить сегмент?')) {
                Segments.remove(itemId);
            }
        }
        else if ($(event.currentTarget).hasClass('edit')){
            $item.addClass('edit');
            $(".item-edit", $item).focus();
        }
    }
});
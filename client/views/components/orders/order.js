import { Template } from 'meteor/templating';
import { $ }        from 'meteor/jquery';
import { selectItem } from './helpers';


Template.order.onRendered(function() {
    this.$(".dropdown").dropdown();
    if ($('#select_all_checkboxes').is(":checked")) {
        let checkbox = this.$("input[type=checkbox][data-id]");
        checkbox.prop('checked', 'checked');
        selectItem(checkbox.data('id'), true);
    }
});

Template.order.events({
    'change input[type=checkbox][data-id]'(event) {
        const checked = event.target.checked;
        selectItem(event.target.dataset.id, checked);
        if (!checked) {
            $('#select_all_checkboxes').prop('checked', '')
        }
    }
});


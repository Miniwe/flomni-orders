import { Session }  from 'meteor/session';

let selectItem = (id, flag) => {
    let selected = Session.get('selectedOrders') || [];
    const k = selected.indexOf(id);

    if (flag && k < 0) {
        selected.push(id);
    }
    if (!flag && k > -1 ) {
        selected.splice(k, 1);
    }

    Session.set('selectedOrders', selected);
}

export { selectItem };
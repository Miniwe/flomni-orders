import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

Template.dashboard.onCreated(function(){
    this.ordersCount = new ReactiveVar(0);

    Meteor.call('getOrdersCount', (error, result) => {
        if (error) {
            return new Meteor.Error('cant get meteor count, error ${error}');
        }
        else {
            this.ordersCount.set(result);
        }
    });

});

Template.dashboard.helpers({
    ordersCount() {
        return Template.instance().ordersCount.get();
    }
})
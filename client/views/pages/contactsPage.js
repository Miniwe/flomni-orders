import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { SubsManager } from 'meteor/meteorhacks:subs-manager';
import Contacts from '../../../lib/collections/contactsCollection';

const ContactsSubs = new SubsManager();

Template.contactsPage.onCreated(function() {
    this.ready = new ReactiveVar();
    this.autorun(() => {
        let handle = ContactsSubs.subscribe('contacts', {});
        this.ready.set(handle.ready());
    });
});

Template.contactsPage.helpers({
    contacts() {
        return Contacts.find();
    },
    collectionReady() {
        return Template.instance().ready.get();
    }
});
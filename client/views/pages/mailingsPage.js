import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { SubsManager } from 'meteor/meteorhacks:subs-manager';
import Mailings from '../../../lib/collections/mailingsCollection';

const MailingsSubs = new SubsManager();

Template.mailingsPage.onCreated(function() {
    this.ready = new ReactiveVar();
    this.autorun(() => {
        let handle = MailingsSubs.subscribe('mailings', {});
        this.ready.set(handle.ready());
    });
});

Template.mailingsPage.helpers({
    mailings() {
        return Mailings.find();
    },
    collectionReady() {
        return Template.instance().ready.get();
    }
});
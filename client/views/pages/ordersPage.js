import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';
import { SubsManager } from 'meteor/meteorhacks:subs-manager';
import { selectItem } from '../components/orders/helpers.js';
import Orders from '../../../lib/collections/ordersCollection';

const OrdersSubs = new SubsManager();
const limit = 30;

Template.ordersPage.onCreated(function() {
    let timeout;

    this.ready = new ReactiveVar();
    this.limit = new ReactiveVar(limit);
    this.total = new ReactiveVar(0);

    Meteor.call('getOrdersCount', (error, result) => {
        if (error) {
            return new Meteor.Error('cant get meteor count, error ${error}');
        }
        else {
            this.total.set(result);
        }
    });

    this.autorun(() => {
        let handle = OrdersSubs.subscribe('orders', {}, {limit: this.limit.get()});
        this.ready.set(handle.ready());

    });

    $(window).bind('scroll.orders', () => {
        window.clearTimeout(timeout);
        if (this.total.get() > this.limit.get()) {
            timeout = window.setTimeout(() => {
                let trigger = $("#orders-container").height() + $("#orders-container").offset().top - $(window).height();

                if (trigger < $(window).scrollTop() && this.ready.get()) {
                    this.limit.set(this.limit.get() + limit);
                }
            }, 100);
        }
    });

});

Template.ordersPage.onDestroyed(function() {
    $(window).unbind('scroll.orders');
});

Template.ordersPage.events({
    'change #select_all_checkboxes': function (event) {
        let checked = event.target.checked ? 'checked' : '';
        $.each($("input[type=checkbox][data-id]"), (index, checkbox) => {
            $(checkbox).prop('checked', checked);
            selectItem($(checkbox).data('id'), !!checked);
        });
    }
});

Template.ordersPage.helpers({
    pageTitle: 'Заказы',
    selectedOrders() {
        return Session.get('selectedOrders') && Session.get('selectedOrders').length > 0;
    },
    orders() {
        return Orders.find({}, {
            sort: {
                statusDate: -1
            }
        });
    },
    collectionReady() {
        return Template.instance().ready.get();
    }
});

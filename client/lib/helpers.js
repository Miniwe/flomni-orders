import { Template } from 'meteor/templating';
import { moment }   from 'meteor/momentjs:moment';
import { _ }        from 'lodash';

Template.registerHelper('moment', function (context, block) {
  if (context && context.hash) {
    block = _.cloneDeep(context);
    context = undefined;
  }
  let date = moment(context);

  // Reset the language back to default before doing anything else
  date.localeData('en');

  for (let i in block.hash) {
    if (date[i]) {
      date = date[i](block.hash[i]);
    } else {
      console.log(`moment.js does not support "${i}"`);
    }
  }
  return date;
});

Template.registerHelper('duration', function (context, block) {
  if (context && context.hash) {
    block = _.cloneDeep(context);
    context = 0;
  }
  let duration = moment.duration(context);

  // Reset the language back to default before doing anything else
  duration = duration.localeData('en');

  for (let i in block.hash) {
    if (duration[i]) {
      duration = duration[i](block.hash[i]);
    } else {
      console.log(`moment.js duration does not support "${i}"`);
    }
  }
  return duration;
});

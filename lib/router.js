import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

FlowRouter.route('/', {
    name: 'index',
    action(params, queryParams) {
        BlazeLayout.render('mainLayout', { main: "indexPage" });
    }
});

FlowRouter.route('/orders', {
    name: 'orders',
    action(params, queryParams) {
        BlazeLayout.render('mainLayout', { main: "ordersPage" });
    }
});

FlowRouter.route('/contacts', {
    name: 'contacts',
    action(params, queryParams) {
        BlazeLayout.render('mainLayout', { main: "contactsPage" });
    }
});

FlowRouter.route('/mailings', {
    name: 'mailings',
    action(params, queryParams) {
        BlazeLayout.render('mainLayout', { main: "mailingsPage" });
    }
});

FlowRouter.notFound = {
    action() {
        BlazeLayout.render('mainLayout', { main: "notFoundPage" });
    }
};

const orderStatus = {
    Draft: 'Черновик',
    New: 'Новый',
    Started: 'В процессе',
    Finished: 'Завершен',
    Completed: 'Закрыт',
    Deleted: 'Удален'
};

const OrderSchema = new SimpleSchema({
    name: {
        type: String,
        label: 'ФИО'
    },
    summ: {
        type: Number,
        decimal: true,
        defaultValue: 0,
        label: 'Сумма'
    },
    town: {
        type: String,
        label: 'Город доставки'
    },
    address: {
        type: String,
        label: 'Адрес доставки'
    },
    shop: {
        type: String,
        label: 'Магазин'
    },
    status: {
        type: String,
        label: 'Статус',
        defaultValue: orderStatus.New,
        allowedValues: Object.keys(orderStatus)
    },
    statusDate: {
        type: Date,
        label: 'Дата изменения статуса',
        autoValue: function () {
            if (this.isInsert) {
                return new Date;
            }
            if (this.isUpsert) {
                return {$setOnInsert: new Date};
            }
        }
    }
});

export default OrderSchema;

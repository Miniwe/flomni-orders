const SegmentSchema = new SimpleSchema({
    name: {
        type: String,
        label: 'Название',
        optional: false

    }
});

export default SegmentSchema;

import { _ } from 'lodash';

export default class Segment {
    constructor(doc) {
        _.extend(this, doc);
    }
}

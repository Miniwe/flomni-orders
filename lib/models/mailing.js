import { _ } from 'lodash';

export default class Mailing {
    constructor(doc) {
        _.extend(this, doc);
    }
}

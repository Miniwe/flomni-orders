import { _ } from 'lodash';

export default class Order {
    constructor(doc) {
        _.extend(this, doc);
    }
}

import { _ } from 'lodash';

export default class Contact {
    constructor(doc) {
        _.extend(this, doc);
    }
}

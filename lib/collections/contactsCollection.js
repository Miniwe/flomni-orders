import Concat from '../models/contact';
import ConcatSchema from '../schemas/contactSchema';

let Concats = new Mongo.Collection("contacts", {
    transform(item) {
        return new Concat(item);
    }
});

Concats.attachSchema(ConcatSchema);

export default Concats;
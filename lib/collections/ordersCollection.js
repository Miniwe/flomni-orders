import Order from '../models/order';
import OrderSchema from '../schemas/orderSchema';

let Orders = new Mongo.Collection("orders", {
    transform(item) {
        return new Order(item);
    }
});

Orders.attachSchema(OrderSchema);

export default Orders;
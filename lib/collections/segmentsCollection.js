import Segment from '../models/segment';
import SegmentSchema from '../schemas/segmentSchema';

let Segments = new Mongo.Collection("segments", {
    transform(item) {
        return new Segment(item);
    }
});

Segments.attachSchema(SegmentSchema);

Segments.allow({
  insert() {return true},
  update() {return true},
  remove() {return true}
});

export default Segments;
import Mailing from '../models/mailing';
import MailingSchema from '../schemas/mailingSchema';

let Mailings = new Mongo.Collection("mailings", {
    transform(item) {
        return new Mailing(item);
    }
});

Mailings.attachSchema(MailingSchema);

export default Mailings;